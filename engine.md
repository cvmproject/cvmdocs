---
title: CVMEngine
description: 
published: true
date: 2023-08-21T03:37:33.187Z
tags: 
editor: markdown
dateCreated: 2023-08-20T19:42:07.422Z
---

# CVMEngine
CVMEngine is a 2D modular real-time multiplayer game engine which will power all of the games in CVMProject.  

If you are looking to use the engine to make a game, a game addon or mod and are unsure where to start, begin with reading through the engine **[Concepts](/engine/concepts)**

At the heart of the engine is a java application based on **[libGDX](https://libgdx.badlogicgames.com/)** which handles all the hard work.  
A lot of the engine's optional functionality comes as **[Modules](/engine/concepts/modules.md)** bundled with CVMEngine, you can enable and disable them at will

[CVMEngine is open-source, click here to check it out!](https://gitlab.com/cvmproject/CVMEngine)
