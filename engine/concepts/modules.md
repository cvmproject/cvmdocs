---
title: Modules
description: 
published: true
date: 2023-08-21T03:35:36.492Z
tags: 
editor: markdown
dateCreated: 2023-08-21T03:14:25.484Z
---

# Modules
## What is a module?
A module is a package that represents a portion of the game that can be either included or ommited (or in some cases even replaced). 


If all dependencies are satisfied, the module should work correctly (unless there are conflicts). Every file that the module needs has to be included in the dependencies or the module itself.

<span style="color:orange">Note: module dependencies are not currently implemented</span>

A great example of a module is a mod that adds a new weapon to the game. That module will have the game as its dependency, and will contain the weapon's textures, sounds, animations, code that defines the weapon and its abilities, as well as some game-dependent code that registers the weapon to be available in the base game.  

Games are simply modules that run some code to load the game on start. For instance, a game module can have code that responds to a "all modules loaded" event by displaying the game menu.  

It is possible to make a game that consists of several modules itself, if it makes sense. For example, a racing game can have all cars defined in a separate module and expose an api to register cars that the module will use. This will not only allow a modder to add more cars to the game, but also will allow the player to disable the cars from the base game, so he can play with modded cars only.

Modules can also be used as a way to introduce namespaces. Global variables are visible across the entire module, but have to be accessed from a different module by referencing the module name.  

#### Examples of proper and incorrect modular system usage
- :white_check_mark: A small game entirely contained within one module
`This is not a problem. Splitting your game into multiple modules should only be done if it makes sense.`
- :white_check_mark: A twin stick shooter with weapons, gamemodes and maps in separate modules 
`This lets modders easily disable weapons/gamemodes/maps from the base game and make an overhaul mod`
- :x: A linear story-based game with levels split between modules 
`BAD IDEA, if you disable a level you can't play through the game anymore and the story breaks. Separating into different modules doesn't make sense.`

- :white_check_mark: An addon which adds a new weapon and a new map which uses the weapon in an interesting way packaged as a single module
`Again, you don't need to split your code into modules if there's not much reason to do it`
- :white_check_mark: An addon that introduces a new gamemode that can be played on existing maps, and a map made specifically for this gamemode in two separate modules 
- :x: An addon that adds a new NPC which only works on one map and the map itself in two modules 
`BAD IDEA, the map won't work without the NPC, and other mappers can't use your NPC since it was written for your map specifically, so it won't appear in the game without the map module`

- :x: An addon which adds a new car for a racing game with the code, textures and sounds all in separate modules 
`BAD IDEA, it never makes sense to load the textures/sounds without loading the car code itself, and the car code obviously requires the textures and sounds. Since they are all related to each other they should be together.`

## Module structure
Modules are located in the **modules** directory. Each module is a directory itself.  
Here is an example structure of a module folder:
```
ExampleModule
    |----module.lua
    |----lua
    |----sounds
    |----fonts
    |----img
    .... (other directories as needed)
```

### Module metadata
Each module has a file called **module.lua** in the module directory.  
This file contains the module metadata:
```lua
MODULE.name = "Example module" -- a human-readable name for the module    
MODULE.author = "awesomeModder1337" -- give yourself credit
MODULE.priority = 4 -- the value that defines the order in which the modules are to be loaded  
```

#### ID
A module's ID is the name of the module directory. Each module, obviously, must have a unique ID. It's a good idea to use prefixes for the module id if they are all part of the same package. 
For example, if I was to make a racing game called "my racing game", I could create modules such as:
- `myracinggame`
- `myracinggame_cars`
- `myracinggame_maps`
- `myracinggame_ai`
etc
### Module contents
A module can contain:

- Game code
- Sounds
- Textures
- Shaders
- Anything else that your code needs

There are currently no restrictions on filetypes.

### Module files and the Virtual File System
CVMEngine effectively merges all the module directories into one large file tree. Whenever a file is referenced by its relative path, that file is searched for in every module's directory. 

For example, if there's a file in `modules/myracinggame_cars/sounds/audi_r8/engine_rev.ogg`, CVMEngine will see it under `sounds/audi_r8/engine_rev.ogg` and since `sound.PlaySound` asks for a path relative to the `sounds` directory, I would play it using 
```lua
sound.PlaySound("audi_r8/engine_rev.ogg")
```

#### File Uniqueness
As a consequence of the above, it is important that every file is unique across all modules.  
For example:
```
modules
|----Pistol
|    |----module.lua
|    |----sounds
|         |----fire.wav
|    ....
|----SMG
|    |----module.lua
|    |----sounds
|         |----fire.wav
     ....
```
Depending on the order of the modules (as defined by the module **priority**, see above), one of the modules will have loaded after the over. The latter module will **override** the files that have similar paths in the former module.  

This is done deliberately, to allow modders to easily create texture/sound/shader packs. However, if you don't want this behavior, you must create some additional folders to prevent collisions:
```
modules
|----Pistol
|    |----module.lua
|    |----sounds
|         |----pistol
|              |----fire.wav
|              |----reload.wav
|----SMG
|    |----module.lua
|    |----sounds
|         |----SMG
|              |----fire.wav
|              |----reload.wav
```

## Module initialization
Modules get loaded on engine startup.  

1. The **modules** directory is scanned, and for every module found the **module.lua** file is loaded
2. The list of modules to load is assembled
3. For every module in order of priority the **autorun** lua files are executed **in lexicographical order**
4. All entity definitions are loaded **together**, the module's priority has no effect (this is to support dynamic reloads in the future)

The main thing that most modules will contain is game code. Refer to [Game Code](./gamecode) for more info on writing it.
