---
title: Concepts: Networking
description: 
published: true
date: 2023-08-20T19:36:48.710Z
tags: 
editor: markdown
dateCreated: 2023-08-20T19:36:47.324Z
---

# Networking

CVMEngine is technically **multiplayer-only**. This means that even if you were to make a singleplayer game (which you can), a server thread is still started in the background and the game client is still technically connected to a server, so all the rules described in here **still apply in singleplayer**  


### Sided code

You may have noticed little pieces of colored text like these in this manual: 
<span style="font-weight: 700;color: yellow">Clientside</span>
<span style="font-weight: 700;color: cyan">Serverside</span>
<span style="font-weight: 700;color: yellow">Sha<span style="color: cyan">red</span></span>  
These pieces of text are there to indicate which *realm* is the function/feature applicable to.  
For example: 
- Only the client has a [GameStart](/engine/api/events/game_start) event, because servers go straight to loading a world. 
- Only the server has a [WorldCreated](/engine/api/events/world_created) event, because clients only ever receive world info from the server, never create it.
- Both the client and server have a [ModulesLoaded](/engine/api/events/modules_loaded) event, because they both load modules on startup

The client and server are **always independent**, even if you are hosting a local game. This means that, for example, even when you load up the game and start a world in singleplayer, [ModulesLoaded](/engine/api/events/modules_loaded) will be called at least **twice** - at least once for the client and once for the server.

Trying to access a feature only accessible from a different realm (for example trying to use the [draw](/engine/api/global/draw) library serverside) will result in an error.

Depending on where you put a lua file, it may only be run on one *realm*. See [Autorun](/engine/concepts/game_code) for an example. 

### Synchronizing client and server

If you were to run a piece of code that sets a variable on a server, it doesn't affect anything on the client. Remember: client and server are **always independent**! Keep important entity state that the client should know about in [DataTables](/engine/concepts/entities/datatables)! However, CVMEngine automatically synchronizes the following things:

###### From SERVER to CLIENT
- Entities getting spawned, removed, saved and loaded
- Positions and velocities of entities
- Entity DataTables (You have to tell CVMEngine which values to synchronize, refer to [Entity DataTables](/engine/concepts/entities/datatables))
- Players connecting, disconnecting, their name and ping changes
- Various server information (time scale, whether the game is paused, etc)
- Information about the various sound events (sounds being played, stopped, looped, slowed down, etc)

###### From CLIENT to SERVER
- Player inputs (See [Input](/engine/api/global/input))
- Various client information (desired interpolation delay, username, player id, etc)

If you need to send additional data as a one-time message, you can use the [Network Library](/engine/api/global/net)

