---
title: CVMEngine API
description: 
published: true
date: 2023-08-20T19:31:53.025Z
tags: 
editor: markdown
dateCreated: 2023-08-20T19:24:27.492Z
---

# CVMEngine API
Welcome to the CVMEngine API reference!

The reference is split into several sections:
- The [Global API](/engine/api/core) section contains references to all the available global tables/libraries for you to use
- The [Entity API](/engine/api/entity) section contains references to all the available functions and properties of [entities](/engine/concepts/entities)
- The [Player API](/engine/api/player) section contains references to all the available functions and properties of [player objects](/engine/concepts/players)
