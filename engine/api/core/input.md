# input
<span style="color:red">??? NO DESCRIPTION ???</span>

## Bind
<span style="color:red">??? </span>input.Bind(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## ControllerHaptic
<span style="color:red">??? </span>input.ControllerHaptic(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## Define
<span style="color:red">??? </span>input.Define(<span style="color:red">???</span>) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## GetControllerAxis
<span style="color:red">??? </span>input.GetControllerAxis(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## IsButtonDown
<span style="color:red">??? </span>input.IsButtonDown(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## IsControllerButtonDown
<span style="color:red">??? </span>input.IsControllerButtonDown(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## IsLMBPressed
<span style="color:red">??? </span>input.IsLMBPressed(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## IsMMBPressed
<span style="color:red">??? </span>input.IsMMBPressed(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## IsRMBPressed
<span style="color:red">??? </span>input.IsRMBPressed(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## IsTouched
<span style="color:red">??? </span>input.IsTouched(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## UIPointerPos
<span style="color:red">??? </span>input.UIPointerPos(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## WasButtonPressed
<span style="color:red">??? </span>input.WasButtonPressed(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## WorldPointerPos
<span style="color:red">??? </span>input.WorldPointerPos(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_A
input.KEY_A - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_B
input.KEY_B - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_C
input.KEY_C - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_CAPS_LOCK
input.KEY_CAPS_LOCK - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_D
input.KEY_D - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_DOWN
input.KEY_DOWN - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_E
input.KEY_E - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_F
input.KEY_F - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_G
input.KEY_G - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_H
input.KEY_H - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_I
input.KEY_I - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_INSERT
input.KEY_INSERT - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_J
input.KEY_J - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_K
input.KEY_K - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_L
input.KEY_L - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_LEFT
input.KEY_LEFT - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_M
input.KEY_M - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_N
input.KEY_N - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_O
input.KEY_O - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_P
input.KEY_P - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_Q
input.KEY_Q - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_R
input.KEY_R - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_RIGHT
input.KEY_RIGHT - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_S
input.KEY_S - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_SPACE
input.KEY_SPACE - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_T
input.KEY_T - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_TAB
input.KEY_TAB - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_U
input.KEY_U - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_UP
input.KEY_UP - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_V
input.KEY_V - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_W
input.KEY_W - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_X
input.KEY_X - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_Y
input.KEY_Y - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### KEY_Z
input.KEY_Z - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

