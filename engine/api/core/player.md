# player
<span style="color:red">??? NO DESCRIPTION ???</span>

## CreateBot
<span style="color:red">??? </span>player.CreateBot(<span style="color:red">???</span>) <span style="color:cyan">Server</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## GetAll
<span style="color:red">??? </span>player.GetAll(<span style="color:red">???</span>) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## GetAllActive
<span style="color:red">??? </span>player.GetAllActive(<span style="color:red">???</span>) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## GetByID
<span style="color:red">??? </span>player.GetByID(<span style="color:red">???</span>) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### STATE_ACTIVE
player.STATE_ACTIVE - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### STATE_CONNECTING
player.STATE_CONNECTING - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### STATE_MISSING
player.STATE_MISSING - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

#### STATE_OFFLINE
player.STATE_OFFLINE - <span style="color:lime">Constant</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

