# draw
This library is the main way to draw stuff on screen, both in the world and on the UI.

### Where can I use this?
This library is a little special, since where the resulting images are going to end up depends on where you use this library.
You need to call these functions every frame to keep seeing the image on the screen.
You should probably only use this library in methods and hooks that have "Draw" in their name. For example:
- [ENT:Draw()](/engine/api/entities/draw.md)
- [UIDraw hook](/engine/api/events/UIDraw.md)
- [Draw hook](/engine/api/events/Draw.md)


## Circle
<span style="color:#3c94e7"></span> draw.Circle(<span style="color:#3c94e7">Number</span> x, <span style="color:#3c94e7">Number</span> y, <span style="color:#3c94e7">Number</span> radius) <span style="color:yellow">Client</span>

Draws a filled circle, see example in `draw.SetColor`


## Line
<span style="color:#3c94e7"></span> draw.Line(<span style="color:#3c94e7">Number</span> x, <span style="color:#3c94e7">Number</span> y, <span style="color:#3c94e7">Number</span> x2, <span style="color:#3c94e7">Number</span> y2, <span style="color:#3c94e7">Number</span> line_width) <span style="color:yellow">Client</span>

Draws a line from ```Vector(x, y)``` to ```Vector(x2, y2)``` which is `line_width` pixels thick


## ProjectUIToScreen
<span style="color:red">??? </span>draw.ProjectUIToScreen(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## ProjectWorldToScreen
<span style="color:red">??? </span>draw.ProjectWorldToScreen(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## Rect
<span style="color:#3c94e7"></span> draw.Rect(<span style="color:#3c94e7">Number</span> x, <span style="color:#3c94e7">Number</span> y, <span style="color:#3c94e7">Number</span> width, <span style="color:#3c94e7">Number</span> height) <span style="color:yellow">Client</span>

Draws an axis-aligned filled rectangle, see example in `draw.SetColor`


## ResetShader
<span style="color:red">??? </span>draw.ResetShader(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## SetColor
<span style="color:#3c94e7"></span> draw.SetColor(<span style="color:#3c94e7">Color</span> color) <span style="color:yellow">Client</span>

Sets the color that will be used to draw the following shapes. You don't have to call it every time you want to draw a
shape, only when the color needs to be changed

```lua
draw.SetColor(Color(255, 0, 0)) -- Sets the shape drawer color to red
draw.Rect(-5, -5, 10, 10) -- Draws a 10x10 red rectangle in the middle of the world
draw.Circle(5, 0, 10) -- Adds a circle shape to the rectangle, the circle is also red because the color did not change
```


## SetShader
<span style="color:red">??? </span>draw.SetShader(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## SetShaderUniform
<span style="color:red">??? </span>draw.SetShaderUniform(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## Sprite
<span style="color:#3c94e7"></span> draw.Sprite(<span style="color:#3c94e7">String</span> asset_name, <span style="color:#3c94e7">Number</span> x, <span style="color:#3c94e7">Number</span> y, <span style="color:#3c94e7">Number</span> width, <span style="color:#3c94e7">Number</span> height, <span style="color:#3c94e7">Number</span> rotation=0, <span style="color:#3c94e7">Number</span> originX=width/2, <span style="color:#3c94e7">Number</span> originY=height/2, <span style="color:#3c94e7">Number</span> alpha=1) <span style="color:yellow">Client</span>

Draws a sprite on the screen.
Only `asset_name`, `x`, `y`, `width` and `height` are required parameters. The rest are optional.
**This method will likely change soon**


## Text
<span style="color:#3c94e7"></span> draw.Text(<span style="color:#3c94e7">String</span> text, <span style="color:#3c94e7">String</span> font, <span style="color:#3c94e7">Number</span> size, <span style="color:#3c94e7">Number</span> x, <span style="color:#3c94e7">Number</span> y, <span style="color:#3c94e7">Number</span> r, <span style="color:#3c94e7">Number</span> g, <span style="color:#3c94e7">Number</span> b, <span style="color:#3c94e7">Number</span> a) <span style="color:yellow">Client</span>

Draws some text on the screen.
**This method will likely change soon**


## UIScreenBottom
<span style="color:red">??? </span>draw.UIScreenBottom(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## UIScreenHeight
<span style="color:red">??? </span>draw.UIScreenHeight(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## UIScreenLeft
<span style="color:red">??? </span>draw.UIScreenLeft(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## UIScreenRight
<span style="color:red">??? </span>draw.UIScreenRight(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## UIScreenTop
<span style="color:red">??? </span>draw.UIScreenTop(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## UIScreenWidth
<span style="color:red">??? </span>draw.UIScreenWidth(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

