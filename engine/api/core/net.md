# net
This library lets the developer send custom (reliable) messages between the client and server.
It's very useful for when the data that CVMEngine sends automatically isn't enough, or for one-off events.
For example, it's used in TSIG and The Void Dogma to send inventory data to clients and to receive inventory move commands from players.

Every network message works like this: one of the parties (client or server) calls net.Start, then optionally calls
net.Write one or several times to add data to the network message, then calls net.Broadcast/net.Send


## AddNetworkString
<span style="color:red">??? </span>net.AddNetworkString(<span style="color:red">???</span>) <span style="color:cyan">Server</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## Broadcast
<span style="color:red">??? </span>net.Broadcast(<span style="color:red">???</span>) <span style="color:cyan">Server</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## GetNetworkString
<span style="color:red">??? </span>net.GetNetworkString(<span style="color:red">???</span>) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## GetNetworkStringId
<span style="color:red">??? </span>net.GetNetworkStringId(<span style="color:red">???</span>) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## Read
<span style="color:red">??? </span>net.Read(<span style="color:red">???</span>) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## Receive
<span style="color:red">??? </span>net.Receive(<span style="color:red">???</span>) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## Send
<span style="color:red">??? </span>net.Send(<span style="color:red">???</span>) <span style="color:cyan">Server</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## SendToServer
<span style="color:red">??? </span>net.SendToServer(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## Start
<span style="color:red">??? </span>net.Start(<span style="color:red">???</span>) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## Write
<span style="color:red">??? </span>net.Write(<span style="color:red">???</span>) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

