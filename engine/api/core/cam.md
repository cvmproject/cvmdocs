# cam
This library lets you control the camera


## GetActualViewSize
<span style="color:#3c94e7">Vector</span> cam.GetActualViewSize() <span style="color:yellow">Client</span>

Returns the size of the actual area being shown. This can be larger than the view size set with `cam.SetViewSize`.


## GetPos
<span style="color:#3c94e7">Vector</span> cam.GetPos() <span style="color:yellow">Client</span>

Returns the camera position set with `cam.SetPos`. Useful if you aren't the one setting it.


## GetViewSize
<span style="color:#3c94e7">Vector</span> cam.GetViewSize() <span style="color:yellow">Client</span>

Returns the view size set with `cam.SetViewSize`. Useful if you aren't the one setting it.


## SetPos
<span style="color:#3c94e7"></span> cam.SetPos(<span style="color:#3c94e7">Vector</span> pos) <span style="color:yellow">Client</span>

Makes the **center** of the camera point at `pos`


## SetViewSize
<span style="color:#3c94e7"></span> cam.SetViewSize(<span style="color:#3c94e7">Vector</span> size) <span style="color:yellow">Client</span>

Sets the size of the minimum area the camera is to show to `size`.
**NOTE**: If the aspect ratio of the game window does not match the aspect ratio of the camera `ViewSize`,
the camera will show **MORE** of the world in the longer axis to avoid stretching the view. Keep that in mind if you want to keep some areas of the world hidden.


