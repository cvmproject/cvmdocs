# console
This library lets you interact with the Developer Console


## AddCommand
<span style="color:#3c94e7"></span> console.AddCommand(<span style="color:#3c94e7">String</span> command, <span style="color:#3c94e7">Function</span> callback) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

Registers a command called `command`. When received, CVMEngine will call `callback` and pass the arguments to the command.

**NOTE**: Commands that you want to run on the server HAVE to be prefixed with `sv_`,
otherwise they won't be passed to the listen server correctly.


## RemoveCommand
<span style="color:#3c94e7"></span> console.RemoveCommand(<span style="color:#3c94e7">String</span> command) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

Unregisters an existing command previously added with `console.AddCommand`.


## RunCommand
<span style="color:#3c94e7"></span> console.RunCommand(<span style="color:#3c94e7">String</span> command_line) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

Runs the command specified. You must specify the whole command line, including the parameters you want in `command_line`

**NOTE**: If you want to run a command on a listen server from the client, the command HAS to be prefixed with `sv_`, otherwise it won't be sent to the listen server.


