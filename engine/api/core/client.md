# client
This library lets you manage the game client


## CommitClientData
<span style="color:red">??? </span>client.CommitClientData(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## ConnectUDP
<span style="color:#3c94e7"></span> client.ConnectUDP(<span style="color:#3c94e7">String</span> address, <span style="color:#3c94e7">Number</span> port) <span style="color:yellow">Client</span>

Connects to a game server at ```address``` on port ```port```
**NOTE:** This function executes immediately, so the code following the call to `client.Connect` may not be run as the client needs to reload LUA to connect!


## Disconnect
<span style="color:red">??? </span>client.Disconnect(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## Exit
<span style="color:red">??? </span>client.Exit(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## IsConnected
<span style="color:red">??? </span>client.IsConnected(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## ReadClientData
<span style="color:red">??? </span>client.ReadClientData(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## StartListenServer
<span style="color:#3c94e7"></span> client.StartListenServer(<span style="color:#3c94e7">Number</span> port, <span style="color:#3c94e7">Table</span> server_params) <span style="color:yellow">Client</span>

Starts (and connects to) a local game server on port `port`, passing the `server_params` as the server parameter table that can later be retrieved by [server.GetServerParams()](server)

```lua
-- Starts a QAR-5 deathmatch game on a map called dm_reload_arena
client.StartListenServer(
        20480,
        {
            map="dm_reload_arena",
            gamemode="dm"
        }
)
```


## StopListenServer
<span style="color:red">??? </span>client.StopListenServer(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## WriteClientData
<span style="color:red">??? </span>client.WriteClientData(<span style="color:red">???</span>) <span style="color:yellow">Client</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

