# physics
This library allows for interaction with the built-in physics engine (Box2D).
Since the physics are only calculated on the server, all functions here are serverside only.


## CreateFrictionJoint
<span style="color:red">??? </span>physics.CreateFrictionJoint(<span style="color:red">???</span>) <span style="color:cyan">Server</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

## QueryAABB
<span style="color:#3c94e7">List<Entity></span> physics.QueryAABB(<span style="color:#3c94e7">Table</span> query_params) <span style="color:cyan">Server</span>

Returns all entities that intersect the provided Axis-Aligned Bounding Box.
`query_params` should be a table, containing the following keys:
- <span style="color:#3c94e7">Vector</span> `pos1` - bottom left corner of the box
- <span style="color:#3c94e7">Vector</span> `pos2` - top right corner of the box
- <span style="color:#3c94e7">Number</span> `mask` - collision mask


## RayCast
<span style="color:#3c94e7">Table</span> physics.RayCast(<span style="color:#3c94e7">Table</span> raycast_params) <span style="color:cyan">Server</span>

Performs a raycast.
`raycast_params` should be a table, containing the following keys:
- <span style="color:#3c94e7">Vector</span> `start_pos` - starting position of the ray
- <span style="color:#3c94e7">Vector</span> `end_pos` - end position of the ray
- <span style="color:#3c94e7">Number</span> `mask` - collision mask for the ray
- <span style="color:#3c94e7">List<Entity></span> `exclude_entities` - a list of entities to ignore collisions with

The method returns a table that will contain the following keys:
- <span style="color:#3c94e7">Boolean</span> `hit` - true if the ray hit something
- <span style="color:#3c94e7">Entity</span> `entity` - the entity the ray hit (if any)
- <span style="color:#3c94e7">Vector</span> `point` - where the hit occurred in world coordinates (if hit)
- <span style="color:#3c94e7">Vector</span> `normal` - the hit normal (if hit)

For example, here's a snippet from The Void Dogma that the hitscan bullets use:

```lua
local raycast_params = {
    start_pos=self:GetPos(),
    end_pos=end_point,
    mask=the_void_dogma.COLLISION_GROUP_PRIMARY + the_void_dogma.COLLISION_GROUP_STATIC,
    exclude_entities={self}
}
local cast_result = physics.RayCast(raycast_params)
if cast_result.hit then
    -- deal damage to cast_result.entity, etc etc
end
```


## SetGravity
<span style="color:red">??? </span>physics.SetGravity(<span style="color:red">???</span>) <span style="color:cyan">Server</span>

<span style="color:red">??? NO DOCUMENTATION ???</span>

