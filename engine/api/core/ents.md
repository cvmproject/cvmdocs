# ents
This library allows for interaction with the entity system.
The "Get***" methods can be used for selecting entities that exist in the world,
while the "Create***" and "Spawn" methods are used to create new entities.


## Create
<span style="color:#3c94e7">Entity</span> ents.Create(<span style="color:#3c94e7">String</span> type) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

Creates and returns an entity with the specified type.
After this method returns, the entity has been initialized, but *has not been spawned yet*.
After setting all the required parameters, you *must* spawn the entity, or it will not appear.


## GetAllActive
<span style="color:#3c94e7">List</span> ents.GetAllActive() <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

Returns a list of all active entities
Does *NOT* include entities in unloaded sectors, only in active ones. If you want to access entities in unloaded sectors, you have to use other selection methods
```lua
-- Prints ids of all active entities
for entity in ents.GetAllActive() do
    print(entity:ID())
end
```


## GetByID
<span style="color:#3c94e7">List</span> ents.GetByID(<span style="color:#3c94e7">Number</span> id) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

Returns the entity which has the corresponding id.
This function is efficient, it does not search through all entities and looks up the entity in the entity map instead.
This function works on entities in unloaded sectors


## GetByType
<span style="color:#3c94e7">List</span> ents.GetByType(<span style="color:#3c94e7">String</span> type) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

Returns a list of all entities of the specified type.
This function is efficient, it does not search through all entities and uses a type map instead.
This function includes entities in unloaded sectors.


## GetInRadius
<span style="color:#3c94e7">List</span> ents.GetInRadius(<span style="color:#3c94e7">Vector</span> pos, <span style="color:#3c94e7">Number</span> radius) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

Returns a list of entities in a radius around a certain position
This function is efficient, it does not search through all entities in the world, only looking in sectors around the provided position.
This function includes entities in unloaded sectors


## LoadArea
<span style="color:#3c94e7"></span> ents.LoadArea(<span style="color:#3c94e7">Vector</span> center, <span style="color:#3c94e7">Number</span> radius) <span style="color:cyan">Sha</span><span style="color:yellow">red</span>

Forcibly loads an area of the world. Unless you know what you are doing, you probably don't need this.
<span style="color:orange">WARNING: Using this function to load a huge area can lead to dramatically reduced performance!</span>


## Spawn
<span style="color:#3c94e7"></span> ents.Spawn(<span style="color:#3c94e7">Entity</span> entity) <span style="color:cyan">Server</span>

Spawns the entity in the world.
The entity will automatically appear on the connected clients and will begin updating in the next tick.
```lua
-- Spawn a platform from qar5 at 100, 100. Notice that BOTH ents.Create AND ents.Spawn are required
local platform = ents.Create("qar5_platform")
platform:SetPos(Vector(100, 100))
ents.Spawn(platform)
```


## SpawnClientside
<span style="color:#3c94e7"></span> ents.SpawnClientside(<span style="color:#3c94e7">Entity</span> entity) <span style="color:yellow">Client</span>

Spawns the entity in the world on this client only.
The entity will begin updating in the next tick, but will NOT exist on the server or any other clients.
```lua
-- Spawn a particle from qar5 at 100, 100
-- We use SpawnClientside because the server does not need to know about particles or network them
local particle = ents.Create("qar5_particle")
particle:SetPos(Vector(100, 100))
ents.SpawnClientside(particle)
```


