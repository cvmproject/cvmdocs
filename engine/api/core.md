# CVMEngine Global API
In CVMEngine, there are several global tables (referred to as "libraries" in the engine code and further) that provide you with access to CVMEngine's features.

For example, where applicable, you can use the [player](/engine/api/core/player) library to get the list of all players connected to the server:

```lua
local players = player.GetAllActive()
```

This section contains the API reference to all the available CVMEngine libraries. Note that not all functionality (and, in fact, not all libraries!) are available on both the client and the server. Read the [Concepts: Networking](/engine/concepts/networking) page to understand what this means.


### Available libraries:
#### <span style="color:cyan">Sha</span><span style="color:yellow">red:</span>
- [ents](/engine/api/core/ents)
- [console](/engine/api/core/console)
- [hook](/engine/api/core/hook)
- [input](/engine/api/core/input)
- [net](/engine/api/core/net)
- [player](/engine/api/core/player)
- [sound](/engine/api/core/sound)
#### <span style="color:yellow">Client only</span>:
- [draw](/engine/api/core/draw)
- [cam](/engine/api/core/cam)
- [client](/engine/api/core/client)
#### <span style="color:cyan">Server only</span>:
- [server](/engine/api/core/server)
- [physics](/engine/api/core/physics)
